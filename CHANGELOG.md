# [3.0.0](https://gitlab.com/to-be-continuous/docker/compare/2.7.1...3.0.0) (2022-08-05)


### Bug Fixes

* trigger new major release ([580d3e3](https://gitlab.com/to-be-continuous/docker/commit/580d3e3ef93ab570f30b7c90216301fefbdbf84e))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.7.1](https://gitlab.com/to-be-continuous/docker/compare/2.7.0...2.7.1) (2022-06-20)


### Bug Fixes

* **skopeo:** authenticate with skopeo inspect ([53cf10d](https://gitlab.com/to-be-continuous/docker/commit/53cf10d42aa268650b0b61705f8b241eb3e7d2b4))
* **Trivy:** prefix Trivy report name ([4cec06b](https://gitlab.com/to-be-continuous/docker/commit/4cec06bb5731ced82e3a9fdecfdd82eb54378822))

# [2.7.0](https://gitlab.com/to-be-continuous/docker/compare/2.6.0...2.7.0) (2022-05-20)


### Features

* **Trivy:** allow Trivy to be run in standalone mode ([88348c8](https://gitlab.com/to-be-continuous/docker/commit/88348c8bab10eaf7b2e732ee6c018f50f587af9e))

# [2.6.0](https://gitlab.com/to-be-continuous/docker/compare/2.5.0...2.6.0) (2022-05-19)


### Features

* Make the --vuln-type Trivy argument configurable ([15457c6](https://gitlab.com/to-be-continuous/docker/commit/15457c6e8574b2f4ab4c22163f11118382fa27a2))

# [2.5.0](https://gitlab.com/to-be-continuous/docker/compare/2.4.0...2.5.0) (2022-05-01)


### Features

* configurable tracking image ([b91e936](https://gitlab.com/to-be-continuous/docker/commit/b91e936e028c8002edd8f79beb20d7451c87ead4))

# [2.4.0](https://gitlab.com/to-be-continuous/docker/compare/2.3.3...2.4.0) (2022-04-27)


### Features

* add image digest support ([57998b2](https://gitlab.com/to-be-continuous/docker/commit/57998b26c37086faee0b5524d31917f4f4a3ce53))

## [2.3.3](https://gitlab.com/to-be-continuous/docker/compare/2.3.2...2.3.3) (2022-04-12)


### Bug Fixes

* **Trivy:** restore default Git strategy to allow .trivyignore ([b2fc514](https://gitlab.com/to-be-continuous/docker/commit/b2fc51491d875d65a4998dfa47f124ba11a50615))

## [2.3.2](https://gitlab.com/to-be-continuous/docker/compare/2.3.1...2.3.2) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([b7ac60a](https://gitlab.com/to-be-continuous/docker/commit/b7ac60a595f6a27b3fc0d24c10465ac923b196d0))

## [2.3.1](https://gitlab.com/to-be-continuous/docker/compare/2.3.0...2.3.1) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([4949a87](https://gitlab.com/to-be-continuous/docker/commit/4949a874fb96dedceb0fb16dcd78693d6351dec0))

# [2.3.0](https://gitlab.com/to-be-continuous/docker/compare/2.2.0...2.3.0) (2022-01-10)


### Features

* Vault variant + non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([f1bbac3](https://gitlab.com/to-be-continuous/docker/commit/f1bbac38b305f8bdda946d8372338c5293da7f59))

# [2.2.0](https://gitlab.com/to-be-continuous/docker/compare/2.1.2...2.2.0) (2021-11-24)


### Features

* add configurable metadata variable with OCI recommended labels ([d3630f9](https://gitlab.com/to-be-continuous/docker/commit/d3630f9356ba3c9934f972ca728f562e3d015019))

## [2.1.2](https://gitlab.com/to-be-continuous/docker/compare/2.1.1...2.1.2) (2021-10-19)


### Bug Fixes

* **trivy:** ignore unfixed security issues by default ([f9a1602](https://gitlab.com/to-be-continuous/docker/commit/f9a160201fe11a1ac7d125b5fa9aa181c4599fd5))

## [2.1.1](https://gitlab.com/to-be-continuous/docker/compare/2.1.0...2.1.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([5596daf](https://gitlab.com/to-be-continuous/docker/commit/5596daf34cc9c64d73405ef490fe75c7ae50a177))

## [2.0.0](https://gitlab.com/to-be-continuous/docker/compare/1.2.3...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([f74d9ef](https://gitlab.com/to-be-continuous/docker/commit/f74d9ef2de5b4dc204b519e14c47862ea2b73b33))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.3](https://gitlab.com/to-be-continuous/docker/compare/1.2.2...1.2.3) (2021-07-21)

### Bug Fixes

* variable name ([1aed76f](https://gitlab.com/to-be-continuous/docker/commit/1aed76f287ff27e35233941e35cbac1e8ea9d2ff))

## [1.2.2](https://gitlab.com/to-be-continuous/docker/compare/1.2.1...1.2.2) (2021-07-05)

### Bug Fixes

* update skopeo credentials ([559961f](https://gitlab.com/to-be-continuous/docker/commit/559961f9f3c30e241a049ee4ea94e9050be49592))

## [1.2.1](https://gitlab.com/to-be-continuous/docker/compare/1.2.0...1.2.1) (2021-06-25)

### Bug Fixes

* permission on reports directory ([2d2f360](https://gitlab.com/to-be-continuous/docker/commit/2d2f360420b13bbdb71c11910ac8214b74a15901))

## [1.2.0](https://gitlab.com/to-be-continuous/docker/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([405fdcc](https://gitlab.com/to-be-continuous/docker/commit/405fdcc65ef1e95cb5997c610266c1422a06802e))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/docker/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([fc9a1ad](https://gitlab.com/Orange-OpenSource/tbc/docker/commit/fc9a1adc498209ea9fa7c6eb64831f6e1abe857f))

## 1.0.0 (2021-05-06)

### Features

* initial release ([0b00fba](https://gitlab.com/Orange-OpenSource/tbc/docker/commit/0b00fba9a515d4ea10c35cf38abc01c217f0979a))
